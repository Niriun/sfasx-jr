﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Game : MonoBehaviour
{
    [SerializeField] private Camera MainCamera;
    [SerializeField] private Character Character;
    [SerializeField] private Canvas Menu;
    [SerializeField] private Canvas Hud;
    [SerializeField] private Transform CharacterStart;
    [SerializeField] private Shader baseShader;
    [SerializeField] private Shader selectShader;

    private RaycastHit[] mRaycastHits;
    private Character mCharacter;
    private Environment mMap;

    private readonly int NumberOfRaycastHits = 1;

    private Transform tile;
    void Start()
    {
        mRaycastHits = new RaycastHit[NumberOfRaycastHits];
        mMap = GetComponentInChildren<Environment>();
        mCharacter = Instantiate(Character, transform);
        ShowMenu(true);
    }


    private void Update()
    {
        // Check to see if the player has clicked a tile and if they have, try to find a path to that 
        // tile. If we find a path then the character will move along it to the clicked tile. 

        if (tile != null)
        {
            tile.gameObject.GetComponent<MeshRenderer>().materials[1].shader = baseShader;
        }

        Ray screenClick = MainCamera.ScreenPointToRay(Input.mousePosition);
        int hits = Physics.RaycastNonAlloc(screenClick, mRaycastHits);
        if (hits > 0)
        {
            tile = mRaycastHits[0].transform;
            if (tile != null)
            {
                tile.gameObject.GetComponent<MeshRenderer>().materials[1].shader = selectShader;

                if (Input.GetMouseButtonDown(0))
                {
                 
                    Debug.Log(tile.gameObject.name);
                    List<EnvironmentTile> route = mMap.Solve(mCharacter.CurrentPosition, tile.transform.GetComponent<EnvironmentTile>());
                    mCharacter.GoTo(route);
                }
            }

        }
    }
        public void ShowMenu(bool show)
        {
            if (Menu != null && Hud != null)
            {
                Menu.enabled = show;
                Hud.enabled = !show;

                if (show)
                {
                    mCharacter.transform.position = CharacterStart.position;
                    mCharacter.transform.rotation = CharacterStart.rotation;
                    mMap.CleanUpWorld();
                }
                else
                {
                    mCharacter.transform.position = mMap.Start.Position;
                    mCharacter.transform.rotation = Quaternion.identity;
                    mCharacter.CurrentPosition = mMap.Start;
                }
            }
        }

        public void Generate()
        {
            mMap.GenerateWorld();
        }

        public void Exit()
        {
#if !UNITY_EDITOR
        Application.Quit();
#endif
        }
    }


